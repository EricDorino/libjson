/*
 * Copyright (C) 2012, 2016  Éric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef json_parser_h_
#define json_parser_h_
#include <stdio.h>
#include "json.h"

struct json_parser {
	FILE *cin;
	char *buf, *ptr;
	size_t bufsize;
	int (*fgetc)(struct json_parser *);
	int (*ungetc)(int, struct json_parser *);
	struct json *result;
	char *error;
	size_t errorsize;
};

struct json_pair;
struct json_pair_list;
struct json_list;

extern struct json *json_object_new(void), 
	   *json_array_new(void),
	   *json_string_new_nodup(const char *), 
	   *json_int_new(long), 
	   *json_real_new(double), 
	   *json_bool_new(int),
	   *json_null_new(void);
extern struct json_pair *json_pair_new(const char *, struct json *);
extern const char *json_pair_name(struct json_pair *);
extern struct json *json_pair_value(struct json_pair *);
extern struct json_pair_list *json_add_pair(struct json_pair_list *, 
				const char *, struct json *);
extern struct json_list *json_add_value(struct json_list *, struct json *);
extern struct json *json_object_new_data(struct json_pair_list *);
extern struct json *json_array_new_data(struct json_list *);

extern int yyparse(struct json_parser *);

#endif
