/*
 * Copyright (C) 2013,  Eric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef json_h_
#define json_h_
#include <stdio.h>

struct json;

extern struct json *json_new(void);
extern void json_free(struct json *);

extern size_t json_len(struct json *);

extern int json_to_file(struct json *, FILE *);
extern struct json *json_from_file(FILE *, char *, size_t);

extern struct json *json_from_buf(char *, size_t, char *, size_t);

extern size_t json_size(struct json *);
extern struct json *json_at(struct json *, size_t);

extern struct json *json_add_object(struct json *, const char *);
extern struct json *json_add_array(struct json *, const char *);

extern void json_add_string(struct json *, const char *, const char *);
extern void json_add_int(struct json *, const char *, long);
extern void json_add_real(struct json *, const char *, double);
extern void json_add_bool(struct json *, const char *, int);

extern int json_find(struct json *, const char *, struct json **);
extern int json_find_string(struct json *, const char *, const char **);
extern int json_find_int(struct json *, const char *, long *);
extern int json_find_real(struct json *, const char *, double *);
extern int json_find_bool(struct json *, const char *, int *);

extern struct json *json_insert_object(struct json *);
extern struct json *json_insert_array(struct json *);

extern void json_insert_string(struct json *, const char *);
extern void json_insert_int(struct json *, long);
extern void json_insert_real(struct json *, double);
extern void json_insert_bool(struct json *, int);

#endif
