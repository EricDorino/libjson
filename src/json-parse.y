/*
 * Copyright (C) 2012,  Eric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

%define api.pure
%debug
%parse-param {struct json_parser *parser}
%lex-param {struct json_parser *parser}

%{
#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include "json-parser.h"
#include "json.h"
%}

%union {
	struct json *jsonval;
	struct json_pair *pairval;
	struct json_pair_list *pairlistval;
	struct json_list *listval;
	long intval;
	double realval;
	int boolval;
	char *stringval;
}

%token <stringval> STRING 
%token <intval> INT 
%token <realval> REAL 
%token <boolval> TRUE FALSE
%token NUL

%type <stringval> string
%type <boolval> bool
%type <jsonval> array object value
%type <pairlistval> pair_list
%type <pairval> pair
%type <listval> value_list

%{
static int yylex(YYSTYPE *, struct json_parser *);
static void yyerror(struct json_parser *, const char *);
static void error(struct json_parser *r, const char *,...);
%}

%%

json: object { parser->result = $1; YYACCEPT; } ;

object: '{' pair_list '}'	{ $$ = json_object_new_data($2); }
	| '{' '}'				{ $$ = json_object_new(); }
	;

pair_list: pair_list ',' pair {
		 	$$ = json_add_pair($1, json_pair_name($3), json_pair_value($3));	
			free($3);
		 }
	| pair  {
		 	$$ = json_add_pair(NULL, json_pair_name($1), json_pair_value($1));	
			free($1);
		}
	;

pair: string ':' value	{ $$ = json_pair_new($1, $3); }
	;

array: '[' value_list ']'	{ $$ = json_array_new_data($2); }
	| '[' ']'				{ $$ = json_array_new(); }

value_list: value_list ',' value	{ $$ = json_add_value($1, $3); }
	| value	{ $$ = json_add_value(NULL, $1); }
	;

value: object
	| array
	| string	{ $$ = json_string_new_nodup(yylval.stringval); }
	| int		{ $$ = json_int_new(yylval.intval); }
	| real		{ $$ = json_real_new(yylval.realval); }
	| bool		{ $$ = json_bool_new(yylval.boolval); }
	| NUL		{ $$ = json_null_new(); }
	;

string: STRING ;

int: '-' INT	{ yylval.intval = -1L*yylval.intval; }
	| INT
	;

real: '-' REAL	{ yylval.realval = -1.0*yylval.realval; }
	| REAL
	;

bool: TRUE | FALSE ;

%%

struct string {
	int size;
	char *str;
};

static void stringinit(struct string *s)
{
	s->str = malloc(s->size = 128);
	if (s->str)
		s->str[0] = '\0';
}

static void stringadd(struct string *s, int c)
{
	if (s->str) {
		int len = strlen(s->str);

		if (len+1 >= s->size)
			s->str = realloc(s->str, s->size += 32);
		if (s->str) {
			s->str[len] = c;
			s->str[len+1] = '\0';
		}
	}
}

static int get_string(YYSTYPE *yylval, struct json_parser *parser)
{
	struct string s;
	int c;

	stringinit(&s);

	c = parser->fgetc(parser);
	while (c != EOF && c != '"') {
		if (c == '\\') {
			switch (c = parser->fgetc(parser)) {
				case '"': case '\\': case '/': 
					stringadd(&s, c); break;
				case 'b': 
					stringadd(&s, '\b'); break;
				case 'f': 
					stringadd(&s, '\f'); break;
				case 'n': 
					stringadd(&s, '\n'); break;
				case 'r': 
					stringadd(&s, '\r'); break;
				case 't':
					stringadd(&s, '\t'); break;
				case 'u': //FIXME
				default:
					error(parser, "mangled string");
					return 0; 
			}
		}
		else stringadd(&s, c);
		c = parser->fgetc(parser);
	}
	yylval->stringval = s.str;
	return STRING;
}

static int get_number(YYSTYPE *yylval, int c, struct json_parser *parser)
{
	struct string s;

	stringinit(&s);

	while (c >= '0' && c <= '9') {
		stringadd(&s, c);
		c = parser->fgetc(parser);
	}
	
	if (c != '.') {
		parser->ungetc(c, parser);
		yylval->intval = atol(s.str);
		free(s.str);
		return INT;
	}

	stringadd(&s, '.');
	c = parser->fgetc(parser);
	while (c >= '0' && c <= '9') {
		stringadd(&s, c);
		c = parser->fgetc(parser);
	}

	if (c == 'e' || c == 'E') {
		stringadd(&s, c);
		c = parser->fgetc(parser);
		if (c == '+' || c == '-') {
			stringadd(&s, c);
			c = parser->fgetc(parser);
		}
		while (c >= '0' && c <= '9') {
			stringadd(&s, c);
			c = parser->fgetc(parser);
		}
	}
	parser->ungetc(c, parser);
	yylval->realval = atof(s.str);
	free(s.str);
	return REAL;
}

static int get_token(YYSTYPE *yylval, int c, struct json_parser *parser)
{
	int result;
	struct string s;

	stringinit(&s);

	while (c >= 'a' && c <= 'z') {
		stringadd(&s, c);
		c = parser->fgetc(parser);
	}
	parser->ungetc(c, parser);

	if (strcmp(s.str, "true") == 0) {
		yylval->boolval = 1;
		result = TRUE;
	}
	else if (strcmp(s.str, "false") == 0) {
		yylval->boolval = 0;
		result = FALSE;
	}
	else if (strcmp(s.str, "null") == 0) {
		result = NUL;
	}
	else {
		error(parser, "unknown token \"%s\"", s.str);
		result = 0;
	}
	free(s.str);
	return result;
}

int yylex(YYSTYPE *yylval, struct json_parser *parser)
{
	int c;

	for (;;) {
		switch (c = parser->fgetc(parser)) {
			case ' ': case '\t': case '\n': case '\r': case '\f':
				continue;
			case '{': case '}':
			case '[': case ']':
			case ',': case ':':
			case '-':
				return c;
			case '"':
				return get_string(yylval, parser);
			case '0': case '1': case '2': case '3': case '4':
			case '5': case '6': case '7': case '8': case '9':
				return get_number(yylval, c, parser);
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
			case 'g': case 'h': case 'i': case 'j': case 'k': case 'l':
			case 'm': case 'n': case 'o': case 'p': case 'q': case 'r':
			case 's': case 't': case 'u': case 'v': case 'w': case 'x':
			case 'y': case 'z':
				return get_token(yylval, c, parser);
			case EOF:
				return 0;
			default:
				error(parser, "bad character %d '%c'", c, isprint(c) ? c : '.');
				break;
		}
	}
	return 0;
}

void yyerror(struct json_parser *parser, const char *msg)
{
	if (parser->error && strlen(parser->error) == 0)
		strncpy(parser->error, msg, parser->errorsize);
	fprintf(stderr, "json-parser: %s\n", msg);
}

void error(struct json_parser *parser, const char *format,...)
{
	char buf[2048];
	va_list ap;

	va_start(ap, format);
	vsnprintf(buf, sizeof buf, format, ap);
	yyerror(parser, buf);
	va_end(ap);
}
