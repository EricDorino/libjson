/*
 * Copyright (C) 2013,  Eric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include "json-parser.h"
#include "json.h"

#define JSON_STRING		0
#define JSON_INT		1
#define JSON_REAL		2
#define JSON_BOOL		3
#define JSON_NULL		4

#define JSON_OBJECT		5
#define JSON_ARRAY		6

struct json_pair_list;
struct json_list;

struct json {
	char type;
	union {
		const char *stringval;
		long intval;
		double realval;
		int boolval;
		struct json_pair_list *object;
		struct json_list *array;
	};
};


struct json *json_string_new_nodup(const char *value)
{
	struct json *result = malloc(sizeof *result);
	
	if (result) {
		result->type = JSON_STRING;
		result->stringval = value;
	}
	else errno = ENOMEM;
	return result;
}

static struct json *json_string_new(const char *value)
{
	const char *v = strdup(value);

	if (v)
		return json_string_new_nodup(v);
	else {
		errno = ENOMEM;
		return NULL;
	}
}

struct json *json_int_new(long value)
{
	struct json *result = malloc(sizeof *result);
	
	if (result) {
		result->type = JSON_INT;
		result->intval = value;
	}
	else errno = ENOMEM;
	return result;
}

struct json *json_real_new(double value)
{
	struct json *result = malloc(sizeof *result);
	
	if (result) {
		result->type = JSON_REAL;
		result->realval = value;
	}
	else errno = ENOMEM;
	return result;
}

struct json *json_bool_new(int value)
{
	struct json *result = malloc(sizeof *result);
	
	if (result) {
		result->type = JSON_BOOL;
		result->boolval = value;
	}
	else errno = ENOMEM;
	return result;
}

struct json *json_null_new(void)
{
	struct json *result = malloc(sizeof *result);
	
	if (result) {
		result->type = JSON_NULL;
	}
	else errno = ENOMEM;
	return result;
}

struct json *json_object_new(void)
{
	struct json *result = malloc(sizeof *result);

	if (result) {
		result->type = JSON_OBJECT;
		result->object = NULL;
	}
	else errno = ENOMEM;
	return result;
}

struct json *json_object_new_data(struct json_pair_list *value)
{
	struct json *result = malloc(sizeof *result);

	if (result) {
		result->type = JSON_OBJECT;
		result->object = value;
	}
	else errno = ENOMEM;
	return result;
}

struct json *json_array_new(void)
{
	struct json *result = malloc(sizeof *result);

	if (result) {
		result->type = JSON_ARRAY;
		result->array = NULL;
	}
	else errno = ENOMEM;
	return result;
}

struct json *json_array_new_data(struct json_list *value)
{
	struct json *result = malloc(sizeof *result);

	if (result) {
		result->type = JSON_ARRAY;
		result->array = value;
	}
	else errno = ENOMEM;
	return result;
}

static void pair_list_free(struct json_pair_list *);
static void value_list_free(struct json_list *);

static void val_free(struct json *val)
{
	if (val) {
		switch (val->type) {
			case JSON_STRING:
				free((char *)val->stringval);
				break;
			case JSON_OBJECT:
				pair_list_free(val->object);
				break;
			case JSON_ARRAY:
				value_list_free(val->array);
				break;
		}
		free(val);
	}
}

struct json_list {
	struct json *value;
	struct json_list *next;
};

struct json_list *json_add_value(struct json_list *list, struct json *value)
{
	struct json_list *result = malloc(sizeof *result);
	struct json_list *list1 = list;

	if (result) {
		result->value = value;
		result->next = NULL;
		if (!list1)
			return result;
		else {
			while (list1->next)
				list1 = list1->next;
			list1->next = result;
		}
		return list;
	}
	else {
		errno = ENOMEM;
		return NULL;
	}
}

static struct json_list *
insert_value(struct json_list *list, struct json *value)
{
	struct json_list *result = malloc(sizeof *result);

	if (result) {
		result->value = value;
		result->next = list;
		return result;
	}
	else {
		errno = ENOMEM;
		return NULL;
	}
}

void value_list_free(struct json_list *list)
{
	if (list) {
		if (list->next)
			value_list_free(list->next);
		free(list);
	}
}

static size_t list_size(struct json_list *list)
{
	if (!list)
		return 0;
	else 
		return 1 + list_size(list->next);
}

static struct json * 
list_at(struct json_list *list, size_t index)
{
	ssize_t i = index;
		
	while (list && i-- > 0)
		list = list->next;
	if (!list)
		return NULL;
	else
		return list->value;
}

struct json_pair {
	const char *name;
	struct json *value;
};

const char *json_pair_name(struct json_pair *pair)
{
	return pair->name;
}

struct json *json_pair_value(struct json_pair *pair)
{
	return pair->value;
}

struct json_pair *json_pair_new(const char *name, struct json *value)
{
	struct json_pair *result = malloc(sizeof *result);

	if (result) {
		if (!(result->name = strdup(name))) {
			free(result);
			errno = ENOMEM;
			return NULL;
		}
		result->value = value;
	}
	else errno = ENOMEM;
	return result;
}

static void pair_free(struct json_pair *pair)
{
	if (pair) {
		free((char *)pair->name);
		val_free(pair->value);
	}
}

struct json_pair_list {
	struct json_pair *pair;
	struct json_pair_list *next;
};

static struct json_pair *
find_pair(struct json_pair_list *list, const char *name)
{
	if (list) {
		if (strcasecmp(list->pair->name, name) == 0)
			return list->pair;
		else
			return find_pair(list->next, name);
	}
	else return NULL;
}

struct json_pair_list *
json_add_pair(struct json_pair_list *list, const char *name, struct json *value)
{
	struct json_pair *exists;

	if ((exists = find_pair(list, name))) {
		json_free(exists->value);
		exists->value = value;
	}
	else {
		struct json_pair_list *result = malloc(sizeof *result);
		struct json_pair_list *list1 = list;

		if (result) {
			result->pair = json_pair_new(name, value);
			result->next = NULL;
			if (!list1)
				return result;
			else {
				while (list1->next)
					list1 = list1->next;
				list1->next = result;
			}
		}
		else {
			errno = ENOMEM;
			return NULL;
		}
	}
	return list;
}

static struct json_pair_list *
insert_pair(struct json_pair_list *list, const char *name, struct json *value)
{
	struct json_pair *exists;

	if ((exists = find_pair(list, name))) {
		json_free(exists->value);
		exists->value = value;
	}
	else {
		struct json_pair_list *result = malloc(sizeof *result);

		if (result) {
			result->pair = json_pair_new(name, value);
			result->next = list;
			return result;
		}
		else {
			errno = ENOMEM;
			return NULL;
		}
	}
	return list;
}

void pair_list_free(struct json_pair_list *list)
{
	if (list) {
		if (list->next)
			pair_list_free(list->next);
		free(list);
	}
}

static size_t pair_list_size(struct json_pair_list *list)
{
	if (!list)
		return 0;
	else 
		return 1 + pair_list_size(list->next);
}

static struct json * 
pair_list_at(struct json_pair_list *list, size_t index)
{
	ssize_t i = index;
		
	while (list && i-- > 0)
		list = list->next;
	if (!list)
		return NULL;
	else
		return list->pair->value;
}

size_t json_size(struct json *json)
{
	if (json) {
		switch (json->type) {
			case JSON_OBJECT:
				return pair_list_size(json->object);
			case JSON_ARRAY:
				return list_size(json->array);
			default:
				return 1;
		}
	}
	else return 0;
}

struct json *json_at(struct json *json, size_t index)
{
	if (json) {
		switch (json->type) {
			case JSON_OBJECT:
				return pair_list_at(json->object, index);
			case JSON_ARRAY:
				return list_at(json->array, index);
			default:
				return index == 0 ? json : NULL;
		}
	}
	else return NULL;
}

static void escape(FILE* file, const char *val) /*FIXME utf-8  \uxxxx */
{
	fputc('"', file);
	while (val && *val) {
		switch (*val) {
			default:
				fputc(*val, file);
				break;
			case '"': case '\\':
				fputc('\\', file);
				fputc(*val, file);
				break;
			case '\b':
				fputs("\\b", file);
				break;
			case '\f':
				fputs("\\f", file);
				break;
			case '\n':
				fputs("\\n", file);
				break;
			case '\r':
				fputs("\\r", file);
				break;
			case '\t':
				fputs("\\t", file);
				break;
		}
		val++;
	}
	fputc('"', file);
}

int json_to_file(struct json *json, FILE *file)
{
	if (json) {
		switch (json->type) {
			case JSON_STRING:
				escape(file, json->stringval);
				return 0;
			case JSON_INT:
				fprintf(file, "%ld", json->intval);
				return 0;
			case JSON_REAL:
				fprintf(file, "%g", json->realval);
				return 0;
			case JSON_BOOL:
				fprintf(file, "%s", json->boolval?"true":"false");
				return 0;
			case JSON_OBJECT:
				{
					struct json_pair_list *list = json->object;

					fputc('{', file);
					while (list) {
						escape(file, list->pair->name);
						fputc(':', file);
						if (json_to_file(list->pair->value, file) < 0)
							return -1;
						if (list->next)
							fputc(',', file);
						list = list->next;
					}
					fputc('}', file);
					return 0;
				}
			case JSON_ARRAY:
				{
					struct json_list *list = json->array;
					fputc('[', file);
					while (list) {
						if (json_to_file(list->value, file) < 0)
							return -1;
						if (list->next)
							fputc(',', file);
						list = list->next;
					}
					fputc(']', file);
					return 0;
				}
			default:
				errno = ENOSYS;
				return -1;
		}
	}
	else { 
		errno = ENOSYS;
		return -1;
	}
}

static size_t escape_len(const char *val) /*FIXME utf-8  \uxxxx */
{
	size_t len = 2;

	while (val && *val) {
		switch (*val) {
			default:
				len += 1;
				break;
			case '"': case '\\':
			case '\b':
			case '\f':
			case '\n':
			case '\r':
			case '\t':
				len += 2;
				break;
		}
		val++;
	}
	return len;
}

size_t json_len(struct json *json)
{
	if (json) {
		switch (json->type) {
			case JSON_STRING:
				return escape_len(json->stringval);
			case JSON_INT:
				return snprintf(NULL, 0, "%ld", json->intval);
			case JSON_REAL:
				return snprintf(NULL, 0, "%g", json->realval);
			case JSON_BOOL:
				return json->boolval?4:5;
			case JSON_OBJECT:
				{
					size_t len = 0;
					struct json_pair_list *list = json->object;

					while (list) {
						len += escape_len(list->pair->name);
						len += 1;
						len += json_len(list->pair->value);
						if (list->next)
							len += 1;
						list = list->next;
					}
					return len + 2;
				}
			case JSON_ARRAY:
				{
					size_t len = 0;
					struct json_list *list = json->array;

					while (list) {
						len += json_len(list->value);
						if (list->next)
							len += 1;
						list = list->next;
					}
					return len + 2;
				}
			default:
				return 0;
		}
	}
	else 
		return 0;
}

struct json *json_add_object(struct json *json, const char *name)
{
	struct json *result = NULL;

	if (json && json->type == JSON_OBJECT) {
		json->object = insert_pair(json->object, name, result = json_object_new());
	}
	return result;
}

struct json *json_add_array(struct json *json, const char *name)
{
	struct json *result = NULL;

	if (json && json->type == JSON_OBJECT) {
		json->object = insert_pair(json->object, name, result = json_array_new());
	}
	return result;
}

void json_add_string(struct json *json, const char *name, const char *value)
{
	if (json && json->type == JSON_OBJECT) {
		json->object = insert_pair(json->object, name, json_string_new(value));
	}
}

void json_add_int(struct json *json, const char *name, long value)
{
	if (json && json->type == JSON_OBJECT) {
		json->object = insert_pair(json->object, name, json_int_new(value));
	}
}

void json_add_real(struct json *json, const char *name, double value)
{
	if (json && json->type == JSON_OBJECT) {
		json->object = insert_pair(json->object, name, json_real_new(value));
	}
}

void json_add_bool(struct json *json, const char *name, int value)
{
	if (json && json->type == JSON_OBJECT) {
		json->object = insert_pair(json->object, name, json_bool_new(value));
	}
}

struct json *json_insert_object(struct json *json)
{
	struct json *result = NULL;

	if (json && json->type == JSON_ARRAY) {
		json->array = insert_value(json->array, result = json_object_new());
	}
	return result;
}

struct json *json_insert_array(struct json *json)
{
	struct json *result = NULL;

	if (json && json->type == JSON_ARRAY) {
		json->array = insert_value(json->array, result = json_array_new());
	}
	return result;
}

void json_insert_string(struct json *json, const char *value)
{
	if (json && json->type == JSON_ARRAY) {
		json->array = insert_value(json->array, json_string_new(value));
	}
}

void json_insert_int(struct json *json, long value)
{
	if (json && json->type == JSON_ARRAY) {
		json->array = insert_value(json->array, json_int_new(value));
	}
}

void json_insert_real(struct json *json, double value)
{
	if (json && json->type == JSON_ARRAY) {
		json->array = insert_value(json->array, json_real_new(value));
	}
}

void json_insert_bool(struct json *json, int value)
{
	if (json && json->type == JSON_ARRAY) {
		json->array = insert_value(json->array, json_bool_new(value));
	}
}

int json_find(struct json *json, const char *name, struct json **result)
{
	if (json && json->type == JSON_OBJECT) {
		struct json_pair *pair = find_pair(json->object, name);	
		
		if (!pair)
			return 0;
		*result = pair->value;
		return 1;
	}
	return 0;
}

int json_find_string(struct json *json, const char *name, const char **result)
{
	struct json *val;

	if (json_find(json, name, &val) && val->type == JSON_STRING) {
		*result = val->stringval;
		return 1;
	}
	return 0;
}

int json_find_int(struct json *json, const char *name, long *result)
{
	struct json *val;

	if (json_find(json, name, &val) && val->type == JSON_INT) {
		*result = val->intval;
		return 1;
	}
	return 0;
}

int json_find_real(struct json *json, const char *name, double *result)
{
	struct json *val;

	if (json_find(json, name, &val) && val->type == JSON_REAL) {
		*result = val->realval;
		return 1;
	}
	return 0;
}

int json_find_bool(struct json *json, const char *name, int *result)
{
	struct json *val;

	if (json_find(json, name, &val) && val->type == JSON_BOOL) {
		*result = val->boolval;
		return 1;
	}
	return 0;
}

struct json *json_new(void)
{
	return json_object_new();
}

static int file_fgetc(struct json_parser *parser)
{
	return fgetc(parser->cin);
}

static int file_ungetc(int c, struct json_parser *parser)
{
	return ungetc(c, parser->cin);
}

struct json *json_from_file(FILE *cin, char *error, size_t errorsize)
{
	struct json_parser parser;

	parser.cin = cin;
	parser.buf = NULL;
	parser.bufsize = 0;
	parser.fgetc = file_fgetc;
	parser.ungetc = file_ungetc;
	parser.result = NULL;
	memset(parser.error = error, 0 , parser.errorsize = errorsize);

	if (yyparse(&parser) != 0) {
		val_free(parser.result);
		return NULL;
	}
	return parser.result;
}

static int buf_fgetc(struct json_parser *parser)
{
	if (parser->ptr && *parser->ptr &&
			parser->ptr < parser->buf+parser->bufsize)
		return *parser->ptr++;
	else
		return EOF;
}

static int buf_ungetc(int c, struct json_parser *parser)
{
	if (parser->ptr == parser->buf)
		return EOF;
	else {
		parser->ptr -= 1;
		return *parser->ptr;
	}
}

struct json *json_from_buf(char *buf, size_t bufsize,
													 char *error, size_t errorsize)
{
	struct json_parser parser;

	parser.cin = NULL;
	parser.buf = parser.ptr = buf;
	parser.bufsize = bufsize;
	parser.fgetc = buf_fgetc;
	parser.ungetc = buf_ungetc;
	parser.result = NULL;
	if (error)
		memset(parser.error = error, 0 , parser.errorsize = errorsize);

	if (yyparse(&parser) != 0) {
		val_free(parser.result);
		return NULL;
	}
	return parser.result;
}

void json_free(struct json *json)
{
	val_free(json);
}
