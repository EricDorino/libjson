/*
 * Copyright (C) 2012,  Eric Dorino.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "json.h"

int main(void) {
	struct json *json = json_new(), *json1, *json2, *json3, *json4;
	const char *str;
	size_t i;
	FILE *tmp;

	json_add_string(json, "result", "success");
	json_add_string(json, "hello", "world");
	json_add_int(json, "One", 1);
	json_add_int(json, "Two", 2);
	json_add_real(json, "planck", 6.62e-34);
	json_add_bool(json, "vrai", 1);
	json_add_bool(json, "faux", 0);

	if (json_find_string(json, "hello", &str))
		printf("FOUND \"hello\": \"%s\"\n", str);
	else
		puts("NOT FOUND \"hello\"");

	if (json_find_string(json, "olla", &str))
		printf("FOUND \"olla\": \"%s\"\n", str);
	else
		puts("NOT FOUND \"olla\"");

	json_to_file(json, stdout);
	printf("\nlen=%ld\n", json_len(json));

	json1 = json_add_object(json, "an_object");
	json_add_string(json1, "you", "YOU");
	json_add_string(json1, "me", "ME");
	printf("size of \"an_object\" is %ld\n", json_size(json1));

	json2 = json_add_array(json, "an_array");
	json_insert_string(json2, "now");
	json_insert_string(json2, "me");
	json_insert_string(json2, "help");
	printf("size of \"an_array\" is %ld\n", json_size(json2));

	json3 = json_add_array(json, "an_array_2");
	json4 = json_insert_array(json3);
	json_insert_int(json4, 1);
	json_insert_int(json4, 2);
	json4 = json_insert_array(json3);
	json_insert_int(json4, 3);
	json_insert_int(json4, 4);
	json4 = json_insert_array(json3);
	json_insert_int(json4, 5);
	json_insert_int(json4, 6);
	printf("size of \"an_array_2\" is %ld\n", json_size(json3));
	for (i = 0; i < json_size(json3); i++) {
		json_to_file(json_at(json3, i), stdout);
		putchar('\n');
	}

	printf("size of \"<top>\" is %ld\n", json_size(json));
	json_to_file(json, stdout);
	putchar('\n');

	if (!(tmp = fopen("/tmp/json.txt", "w")))
		fputs("cannot open tempfile for writing\n", stderr);
	else {
		json_to_file(json, tmp);
		fputc('\n', tmp);
		fclose(tmp);
		if (!(tmp = fopen("/tmp/json.txt", "r")))
			fputs("cannot open tempfile for reading\n", stderr);
		else {
			char err[255];

			struct json *jsonbis = json_from_file(tmp, err, sizeof(err));
			if (!jsonbis)
				fprintf(stderr, "json parse error: %s\n", err);
			else {
				json_to_file(jsonbis, stdout);
				putchar('\n');
				json_free(jsonbis);
			}
			fclose(tmp);
		}
	}

	
	{
		char err[255];
		char buf[] ="{\"error\":\"boum\",\"array\":[1,2],\"value\":1664}";

		struct json *jsonbis = json_from_buf(buf, sizeof(buf), err, sizeof(err));
		if (!jsonbis)
			fprintf(stderr, "json parse error: %s\n", err);
		else {
			json_to_file(jsonbis, stdout);
			putchar('\n');
			json_free(jsonbis);
		}
	}

	json_free(json);
	return 0;
}
